import psycopg2
from psycopg2.errors import CheckViolation

price_request = "SELECT price FROM Shop WHERE product = %(product)s"
buy_decrease_balance = f"UPDATE Player SET balance = balance - ({price_request}) * %(amount)s WHERE username = %(username)s"
buy_decrease_stock = "UPDATE Shop SET in_stock = in_stock - %(amount)s WHERE product = %(product)s"
add_to_inventory = "INSERT INTO Inventory (username, product, amount) VALUES (%(username)s, %(product)s, %(amount)s) ON CONFLICT ON CONSTRAINT pk DO UPDATE SET amount = Inventory.amount + EXCLUDED.amount"
size_of_inventory = "SELECT sum(amount) FROM Inventory WHERE username = %(username)s"


def get_connection(dbname = "lab"):
    return psycopg2.connect(
        dbname=dbname,
        user="user",
        password="pass",
        host="0.0.0.0",
        port=5432
    )


def init_db():
    conn = get_connection(None)
    conn.autocommit = True
    cur = conn.cursor()

    try:
        cur.execute("CREATE DATABASE lab")

    finally:
        cur.close()
        conn.close()

    with get_connection() as conn, conn.cursor() as cur:
        cur.execute("CREATE TABLE Player (username TEXT UNIQUE, balance INT CHECK(balance >= 0))")
        cur.execute("CREATE TABLE Shop (product TEXT UNIQUE, in_stock INT CHECK(in_stock >= 0), price INT CHECK (price >= 0))")
        cur.execute("CREATE TABLE Inventory (username TEXT, product TEXT, amount INT CHECK (amount >= 0 AND amount <= 100), CONSTRAINT pk PRIMARY KEY (username, product));")
        cur.execute("INSERT INTO Player (username, balance) VALUES ('Alice', 1000)")
        cur.execute("INSERT INTO PLayer (username, balance) VALUES ('Bob', 2000)")
        cur.execute("INSERT INTO Shop (product, in_stock, price) VALUES ('marshmello', 100, 10)")
        cur.execute("INSERT INTO Shop (product, in_stock, price) VALUES ('water', 200, 5)")


def buy_product(username, product, amount):
    obj = {"product": product, "username": username, "amount": amount}
    with get_connection() as conn, conn.cursor() as cur:
        try:
            cur.execute("BEGIN")

            try:
                cur.execute(buy_decrease_balance, obj)
                if cur.rowcount != 1:
                    raise Exception("Wrong username.")

            except CheckViolation:
                raise Exception("Low balance.")

            try:
                cur.execute(buy_decrease_stock, obj)
                if cur.rowcount != 1:
                    raise Exception("Wrong product.")

            except CheckViolation:
                raise Exception("Out of stock.")

            cur.execute(add_to_inventory, obj)
            if cur.rowcount != 1:
                raise Exception("Something wrong with inventory.")

            cur.execute(size_of_inventory, obj)
            if cur.rowcount != 1:
                raise Exception("Something wrong with inventory.")

            if cur.fetchone()[0] > 100:
                raise Exception("Inventory is overflow.")

        except Exception as exception:
            cur.execute("ROLLBACK")
            raise exception


if __name__ == '__main__':
    init_db()
